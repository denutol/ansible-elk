Тестовое задание

Описание задачи:
Необходимо написать ansible-плейбук который:

1.            Установит и выполнит базовую конфигурацию кластера ElasticSearch (две и более нод);
2.            Установит и выполнит базовую конфигурацию Logstash;
3.            Установит и выполнит базовую конфигурацию Kibana;


Запуск


1. Настраиваем сеть для оверлей-сеть для контейнеров

docker network create  --driver overlay --subnet 10.0.9.0/24 overlaynet

2. Запуск контейнеров 


#Elasticsearch
docker service create -p 222:22 --network overlaynet --name server1  ubuntu-test
docker service create -p 223:22 --network overlaynet --name server2  ubuntu-test
docker service create -p 224:22 --network overlaynet --name server3  ubuntu-test
#LOGSTASH+KIBANA
docker service create -p 225:22 -p 5601:5601 --network overlaynet --name server4  ubuntu-test

3. Обновляем файл hosts(внутри контейнеров, нужно для подлкючения нод)

docker inspect -f "{{json .NetworkSettings.Networks.overlaynet.IPAMConfig.IPv4Address}} {{ index .Config.Labels \"com.docker.swarm.service.name\"}}" $(docker ps -q)|tr -d \" > vars/hosts

4. Запуск плейбука

 ansible-playbook -i inventory playbook_es_cluster.yml --ask-pass -vvvvv
 ansible-playbook -i inventory playbook_logstash_kibana.yml --user=root --ask-pass -vvvvv

5. Проверки

curl -XGET 'http://localhost:9200/_cluster/health?pretty=true'
curl -XGET 'http://server1:9200/_cluster/health?pretty=true'
curl -XGET 'http://server1:9200//_cat/indices?v'



Источники:
https://docs.docker.com/engine/examples/running_ssh_service/#run-a-test_sshd-container
https://habr.com/post/305400/
https://rtfm.co.ua/ansible-primer-ustanovki-nginx/
https://docs.docker.com/v17.09/engine/userguide/networking/#overlay-networks-in-swarm-mode
https://stackoverflow.com/questions/28632977/elasticsearch-in-docker-container-cluster
https://discuss.elastic.co/t/trying-to-setup-elasticsearch-cluster-with-docker-compose/106803/9
https://habr.com/company/express42/blog/254959/